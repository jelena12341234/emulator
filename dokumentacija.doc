Drugi deo domaceg zadatka se pokrece preko komandne linije potpisom:
./emulator iv_table.o routines.o ulaz3.o -place=iv_table@0x0000
 Po uslovu zadatka, IV tabela treba poceti na adresi 0x0000, zato postavljanjem drugacije opcije se prijavljuje greska. U svakom od prilozenih testova postoje fajlovi iv_table.o i routines.o, dok se u iv_table nalaze definisane adrese prva cetiri ulaza, u routines.o se nalaze prekidne rutine. Takodje je potrebno imati text sekciju kako bi se znalo odakle treba da se zapocne izvrsavanje programa, u suprotonom program prijavljuje gresku.

U mainu se prvo pamte adrese sekcija koje su navedene u komandnoj liniji (pamcenje se vrsi pomocu klase SecAdr), zatim se poziva linker pa emulator. Linker ima za zadatak da poveze sve navedene objektne fajlove i ucita kod u memoriju pre nego sto emulator pocne sa izvrsavanjem. Linker radi povezivanje tako sto pravi jedinstvenu tabelu simbola od svih ulaznih fajlova, menja pomeraj sekcija i na kraju, pre punjenja memorije, razresava relokacione zapise, kod kojih razlikujemo apsolutne i PC relativne relokativne zapise(oznaceni sa "R_16" i "R_PC16"), (pamcenje se vrsi pomocu Relocs klase). Pored povezivanja procesora i memorije u ovom nasem zamisljenom sistemu, postoje i terminal i tajmer koji prave prekide. U domacem zadatku su realizovani pomocu niti(biblioteke thread). Za terminal je koriscena biblioteka poll.h, pomocu koje je realizovano citanje upisanog znaka, dok postoje jos i funkcije enableRawMode() i disableRawMode() koje imaju za zadatak da onemoguce/omoguce ispis karaktera u konzoli kada se odgovarajuci taster pritisne i generise prekid. Tajmer predstavlja metodu koja se vrti dok se program ne zavrsi i koristi funkciju nalik funkciji sleep. Emulator, glavni deo domaceg zadatka, sadrzi nekoliko kljucnih metoda, exec() cita vrednost memorije na adresi PC i pomocu switch-case razresava o kojoj instrukciji je rec, a u narednim metodama razresava adresiranja i operande. 

Bin fajl je kreiran pomocu makefile-a:

emulator:  *.cpp
	g++ -g -o emulator *.cpp -I. -pthread


Test primeri:
U svakom test primeru se koriste objektni fajlovi iv_table.o i routines.o; iv_table.o kod svakog programa izgleda isto, dok se routines.o razlikuje samo kod prvog testa.  
--------iv_table.o-----------------
<sym_table>
#ime	sek	vr	vel	vid
iv_table	1	0	8	l
first	UND	0	0	g
second	UND	0	0	g
third	UND	0	0	g
fourth	UND	0	0	g
#rel.iv_table
0	R_16	2	 
2	R_16	3	 
4	R_16	4	 
6	R_16	5	 
<iv_table>:
0000
0000
0000
0000
------routines.o------




1. timer
U ovom test primeru se testira period tajmera(da li sve vrednosti periode rade lepo) tako sto se glavni program vrti u beskonacnoj petlji sve dok se ne pritisne neki taster kojim se zavrsava program.

-----------routines.o--------------------
<sym_table>
#ime	sek	vr	vel	vid
iv_table	1	0	15	l
first	1	0	0	g
second	1	5	0	g
third	1	10	0	g
fourth	1	14	0	g
<iv_table>:
6800012a
08
60000a2a
08
6c2228
08
00

------timer.out--------------------
<sym_table>
#ime	sek	vr	vel	vid
text	1	0	33	l
skok	1	0	0	g
#rel.text
31	R_16	2	 
<text>:
6400000022
68000122
70000122
68000122
70000122
68000122
70000122
2c000000

./emulator iv_table.o routines.o timer.o -place=iv_table@0x0000 -place=text@0x1000

2. findMax
U ovom test primeru zadatak je da se prodje kroz niz koji se nalazi u drugom fajlu(array.o) i da se nadje maksimum. Maksimum se cuva u registru r1, tako da je lako proveriti validnost izvrsenja programa.
----------findMax.out--------------------
<sym_table>
#ime	sek	vr	vel	vid
text	1	0	57	l
loop	1	8	0	g
labjedan	1	40	0	g
kraj	1	56	0	g
nizPocetak	UND	0	0	g
nizKraj	UND	0	0	g
#rel.text
10	R_16	5	 
21	R_16	3	 
29	R_16	6	 
34	R_16	2	 
38	R_16	4	 
49	R_16	6	 
54	R_16	2	 
<text>:
60000022
60000024
6400000026
6c2426
8c4622
44000000
68000226
8c00000026
44000800
2c000000
644622
68000226
8c00000026
44000800
00


----------array.out----------------------
<sym_table>
#ime	sek	vr	vel	vid
data	1	0	14	l
nizPocetak	1	0	0	g
nizKraj	1	14	0	g
<data>:
0100
0200
0700
0400
0900
0500
0200
0500

Pokretanje komandom:
./emulator iv_table.o routines.o findMax.o array.o -place=iv_table@0x0000 -place=text@0x1000

Rezultat:

reg[0]=0
reg[1]=9
reg[2]=0
reg[3]=4160
reg[4]=0
reg[5]=1
reg[6]=65280
reg[7]=4146

reg[1] cuva maksimum, reg[3] cuva tekucu poziciju elementa u memoriji(u ovom slucaju, na kraju programa je reg[3] stigao do kraja niza)
reg[6] cuva trenutnu vrednost SP-a, dok PC pokazuje na narednu instrukciju u memoriji(posle halt-a).

Pored ovog ispisa, postoje i drugi ispisi u konzoli koji citaju vrednosti iz tabele simbola izvrsnog programa i o instrukcijama koje se obradjuju.

3. reverse
Zadatak ovog testa jeste da obrne sve elemente niza koji se nalaze u drugom fajlu(array.o). Niz se cuva u memoriji od adrese nizPocetak do nizKraj pa je potrebno ispisati elemente izmedju tih memorijskih lokacija kako bi se proverila validnost programa.

----------reverse.out--------------------
<sym_table>
#ime	sek	vr	vel	vid
text	1	0	39	l
loop	1	14	0	g
nizPocetak	UND	0	0	g
nizKraj	UND	0	0	g
#rel.text
2	R_16	3	 
7	R_16	4	 
36	R_16	2	 
<text>:
6400000022
6400000024
70000224
644226
644442
642644
68000222
70000224
8c2422
44000000
00

----------array.out----------------------
<sym_table>
#ime	sek	vr	vel	vid
data	1	0	14	l
nizPocetak	1	0	0	g
nizKraj	1	14	0	g
<data>:
0100
0200
0700
0400
0900
0500
0200
0500

Pokretanje komandom:
./emulator iv_table.o routines.o findMax.o array.o -place=iv_table@0x0000 -place=text@0x1000


Rezultat:
Ispis u konzoli:

$$$$$NIZ PRE$$$$$$$$$$
1 0
3 0
7 0
4 0
9 0
2 0
5 0


$$$$$NIZ POSLE$$$$$$$$$$
5 0
2 0
9 0
4 0
7 0
3 0
1 0

Niz se stampa direktno iz memorije.
Pored ovog ispisa, postoje i drugi ispisi u konzoli koji citaju vrednosti iz tabele simbola izvrsnog programa i o instrukcijama koje se obradjuju.

4. sort
Zadatak ovog testa jeste da sortira elemente niza, koji se cuvaju od adrese nizPocetak do nizKraj u memoriji. Te vrednosti memorije je potrebno ispisati kako bi se proverila validnost programa.

--------------sort.out-------------------
<sym_table>
#ime	sek	vr	vel	vid
text	1	0	87	l
labela	1	46	0	g
labelab	1	8	0	g
kraj	1	86	0	g
nizPocetak	UND	0	0	g
nizKraj	UND	0	0	g
#rel.text
2	R_16	5	 
13	R_16	2	 
21	R_16	6	 
26	R_16	3	 
34	R_16	5	 
40	R_16	6	 
44	R_16	4	 
61	R_16	6	 
66	R_16	3	 
74	R_16	5	 
80	R_16	6	 
84	R_16	4	 
<text>:
6400000024
642426
8c4644
44000000
68000226
8c00000026
44000000
68000224
6400000026
8c24000000
44000000
64442a
644644
642a46
68000226
8c00000026
44000000
68000224
6c00000026
8c24000000
44000000
00

----------array.out----------------------
<sym_table>
#ime	sek	vr	vel	vid
data	1	0	14	l
nizPocetak	1	0	0	g
nizKraj	1	14	0	g
<data>:
0100
0200
0700
0400
0900
0500
0200
0500


Pokretanje komandom:
./emulator iv_table.o routines.o findMax.o array.o -place=iv_table@0x0000 -place=text@0x1000


Rezultat:
$$$$$NIZ PRE$$$$$$$$$$
1 0
3 0
7 0
4 0
9 0
2 0
5 0

$$$$$NIZ POSLE$$$$$$$$$$
1 0
2 0
3 0
4 0
5 0
7 0
9 0


Niz se stampa direktno iz memorije.
Pored ovog ispisa, postoje i drugi ispisi u konzoli koji citaju vrednosti iz tabele simbola izvrsnog programa i o instrukcijama koje se obradjuju.
