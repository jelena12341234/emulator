.global labela, labelab, kraj
.extern nizPocetak, nizKraj
.section text:

mov $nizPocetak,%r2
mov %r2,%r3

labelab:
cmp *(%r3),*(%r2)
jgt labela
add $2,%r3
cmp $nizKraj,%r3
jgt labelab
add $2,%r2
mov $nizPocetak,%r3
cmp %r2,$nizKraj
jgt kraj

labela:
mov *(%r2),%r5
mov *(%r3),*(%r2)
mov %r5,*(%r3)
add $2,%r3
cmp $nizKraj,%r3
jgt labelab
add $2,%r2
add $nizPocetak,%r3
cmp %r2,$nizKraj
jgt kraj

kraj:
halt
.end