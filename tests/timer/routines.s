.global first, second, third, fourth
.section iv_table:
first:
    add $1,%r5
    iret

second:
    mov $10,%r5
    iret


third:
    add %r1,%r4
    iret

fourth:
    halt

.end
