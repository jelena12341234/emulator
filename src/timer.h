#pragma once
#include <thread>

class Emulator;

class Timer
{
    unsigned short timer_cfg;
public:
    std::thread _thread;

    Timer();

    friend class Emulator;

    int getTimerInterval();

    void timer();
};