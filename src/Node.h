#pragma once
#include <string>

class Node{
public:
	bool local; //true for local false for global
	int value;
	int size;
	static unsigned long int jedId;
	unsigned int serialNum;

	std::string name;	
	std::string UNDsection="x";
	int section;


	Node(std::string n, std::string sec, int val, int sz, char lVg);

	Node(std::string n, int sec, int val, int sz, char lVg);
	
};