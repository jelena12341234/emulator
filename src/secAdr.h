#pragma once
#include <string>

class SecAdr{
    std::string section;
    unsigned long int address;
public:
    SecAdr(std::string sec, unsigned long int adr)
    {
        section = sec;
        address = adr;
    }

    std::string getSection()
    {
        return section;
    }

    unsigned long int getAddress()
    {
        return address;
    }


};