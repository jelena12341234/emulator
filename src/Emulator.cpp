#include "Emulator.h"
#include "linker.h"
#include <thread>
#include <bitset>
#include <iostream>
#include <termios.h>
#include <unistd.h>

bool Emulator::timerIntr = false;
bool Emulator::consoleIntr = false;
bool Emulator::invalidInstr = false;
bool Emulator::halted = false;
char *Emulator::memory = new char[MEM_SYZE];
unsigned short Emulator::timer_cfg = 0;



Emulator::~Emulator()
{
    delete memory;
    if (_timer != nullptr)
    {
        _timer->_thread.detach();
        delete _timer;
    }
    if (_keyboard != nullptr)
    {
        _keyboard->_thread.detach();
        delete _keyboard;
    }

    disableRawMode();

}

Emulator::Emulator()
{
    for (int i = 0; i < 8; i++)
    {
        reg[i] = 0;
    }

    SP = 0xFF00;
    PC = Linker::_rip;	//register instruction pointer
   
    psw = std::bitset<16>(0x0000);
    memory = new char[1 << 16];
    memory = Linker::_fMemory;

    timer_cfg = Emulator::memory[0xff10] | (Emulator::memory[0xff11]) << 8;

    enableRawMode();  //printed characters arent displayed in console
    
 //   _timer = new Timer();
 //   _keyboard = new KeyBoard();
}

void Emulator::exec()
{
    
    while (!halted)
    {
        interrupt();
        int word = memory[PC++];
        int size = word & INSTR_SIZE; //if size ==0 size = 1 else size =2
        int adr1, adr2;               //nacin adr za op1 i adr za op2
        int op1, op2, temp;
        int pos;
        int position;
        char higher, lower;
        bool sgn, sgn1, sgn2;
        int OC = (word & 0xf8) >> 3;
        switch (OC)
        {
        case halt:
            halted = true;
            break;
        case iret:
            interrupted=false;
            lower = memory[SP++]; higher = memory[SP++];
            psw[15] = (higher & 0x80) > 0 ? 1 : 0 ;
            psw[14] = (higher & 0x40) > 0 ? 1 : 0 ;
            psw[13] = (higher & 0x20) > 0 ? 1 : 0 ;
            psw[3] = (higher & 0x08) > 0 ? 1 : 0 ;
            psw[2] = (higher & 0x04) > 0 ? 1 : 0 ;
            psw[1] = (higher & 0x02) > 0 ? 1 : 0 ;
            psw[0] = (higher & 0x01) > 0 ? 1 : 0 ;
             
            PC = memory[SP++] & 0xff  |   (memory[SP++] << 8);
			break;
        case ret:
            interrupted=false;
            std::cout<<"ret recognized: "<<std::endl;

            PC = (memory[SP++] & 0xff ) |   ((memory[SP++] << 8)&0xff);
            break;
        case intr:
            op1 = this->adrressInstr(adr1, pos, size);
            PC = (memory[(op1 % 8)*2] & 0xff ) |   ((memory[(op1 % 8)+1] << 8)&0xff);
			break;
        case call:
			memory[--SP] = (PC >> 8) & 0xff;
            memory[--SP] = PC & 0xff;
			op1 = this->adrressInstr(adr1, pos, size);
            jump(op1);
            break;
        case jmp:
        //    std::cout<<"jmp recognized"<<std::endl;
            op1 = this->adrressInstr(adr1, pos, size);
            if(adr1==3 && pos==7) {PC+=op1; break;}
            jump(op1);
            break;
        case jeq:
            op1 = this->adrressInstr(adr1, pos, size);
            if (psw[Z_FLAG]) jump(op1);
            break;
        case jne:
            op1 = this->adrressInstr(adr1, pos, size);
            if (!psw[Z_FLAG])
                jump(op1);
            break;
        case jgt:   
            if(op1 > op2)
            {
                op1 = this->adrressInstr(adr1, pos, size);
                jump(op1);
            }
            else 
                op1 = this->adrressInstr(adr1, pos, size);
            break;
        case push:
            op1 = this->adrressInstr(adr1, pos, size);

            if (size > 0)
            {
                memory[--SP] = (op1 >> 8) & 0xff;
                memory[--SP] = op1 & 0xff;
            }
            else
                memory[--SP] = op1 & 0xff;
            break;
        case pop:
            op1 = this->adrressInstr(adr1, pos, size);
   
            if (size > 0)
                op1 = (memory[SP++] & 0xff) | ((memory[SP++] >> 8)&0xff);
             else
                op1 = memory[SP++];       

                
            this->setResult(adr1, op1, pos); 
            break;
        case xchg:
            op1 = this->adrressInstr(adr1, pos, size);
            position = pos;
            op2 = this->adrressInstr(adr2, pos, size);
            
            this->setResult(adr2, op1, pos);
            this->setResult(adr1, op2, position);
            break;
        case mov:
            op1 = this->adrressInstr(adr1, pos, size);
            op2 = this->adrressInstr(adr2, pos, size);
            position = pos;

            op2 = op1;

            psw[Z_FLAG] = op2 == 0 ? 1 : 0;
            psw[N_FLAG] = (op2 & 0x8000) ? 1 : 0;

            this->setResult(adr2, op2, position);
            break;
        case add:
            op1 = this->adrressInstr(adr1, pos, size);
            op2 = this->adrressInstr(adr2, pos, size);
            position = pos;
            sgn1 = (op1 & 0x8000) ? 1 : 0;
            sgn2 = (op2 & 0x8000) ? 1 : 0;
            op2 = op1 + op2;
            sgn = (op2 & 0x8000) ? 1 : 0;
            psw[Z_FLAG] = op2 == 0 ? 1 : 0;
            psw[N_FLAG] = (op2 & 0x8000) ? 1 : 0;
            psw[C_FLAG] = (op2 & 0x10000) ? 1 : 0;
            psw[O_FLAG] = ((!sgn && !sgn1 && !sgn2) || (sgn && sgn1 && sgn2)) ? 0 : 1;

            this->setResult(adr2, op2, position);
            break;
        case sub:
            op1 = this->adrressInstr(adr1, pos, size);
            op2 = this->adrressInstr(adr2, pos, size);
            position = pos;
            sgn1 = (op1 & 0x8000) ? 1 : 0;
            sgn2 = (op2 & 0x8000) ? 1 : 0;
            op2 = op2 - op1;
            sgn = (op2 & 0x8000) ? 1 : 0;
            psw[Z_FLAG] = op2 == 0 ? 1 : 0;
            psw[N_FLAG] = (op2 & 0x8000) ? 1 : 0;
            psw[C_FLAG] = (op2 & 0x10000) ? 1 : 0;
            psw[O_FLAG] = ((!sgn && !sgn1 && !sgn2) || (sgn && sgn1 && sgn2)) ? 0 : 1;

            this->setResult(adr2, op2, position);
            break;
        case mul:
            op1 = this->adrressInstr(adr1, pos, size);
            op2 = this->adrressInstr(adr2, pos, size);
            position = pos;

            op2 = op1 * op2;

            psw[Z_FLAG] = op2 == 0 ? 1 : 0;
            psw[N_FLAG] = (op2 & 0x8000) ? 1 : 0;

            this->setResult(adr2, op2, position);
            break;
        case div_:
            op1 = this->adrressInstr(adr1, pos, size);
            op2 = this->adrressInstr(adr2, pos, size);
            position = pos;

            op2 = op2 / op1;
            psw[Z_FLAG] = op2 == 0 ? 1 : 0;
            psw[N_FLAG] = (op2 & 0x8000) ? 1 : 0;

            this->setResult(adr2, op2, position);
            break;
        case cmp:
            op1 = this->adrressInstr(adr1, pos, size);
            op2 = this->adrressInstr(adr2, pos, size);
            sgn1 = (op1 & 0x8000) ? 1 : 0;
            sgn2 = (op2 & 0x8000) ? 1 : 0;
            temp = op2 - op1;
        
            sgn = (temp & 0x8000) ? 1 : 0;
            psw[Z_FLAG] = temp == 0 ? 1 : 0;
            psw[N_FLAG] = (temp & 0x8000) ? 1 : 0;
            psw[C_FLAG] = (temp & 0x10000) ? 1 : 0;
            psw[O_FLAG] = ((!sgn && !sgn1 && !sgn2) || (sgn && sgn1 && sgn2)) ? 0 : 1;

            break;
        case not_:
            op1 = this->adrressInstr(adr1, pos, size);
            op2 = this->adrressInstr(adr2, pos, size);
            position = pos;

            op2 = ~op1;
            psw[Z_FLAG] = op2 == 0 ? 1 : 0;
            psw[N_FLAG] = (op2 & 0x8000) ? 1 : 0;

            this->setResult(adr2, op2, position);
            break;
        case and_:
            op1 = this->adrressInstr(adr1, pos, size);
            op2 = this->adrressInstr(adr2, pos, size);
            position = pos;

            op2 = op2 & op1;
            psw[Z_FLAG] = op2 == 0 ? 1 : 0;
            psw[N_FLAG] = (op2 & 0x8000) ? 1 : 0;

            this->setResult(adr2, op2, position);
            break;
        case or_:
            op1 = this->adrressInstr(adr1, pos, size);
            op2 = this->adrressInstr(adr2, pos, size);
            position = pos;

            op2 = op2 | op1;
            psw[Z_FLAG] = op2 == 0 ? 1 : 0;
            psw[N_FLAG] = (op2 & 0x8000) ? 1 : 0;

            this->setResult(adr2, op2, position);
            break;
        case xor_:
            op1 = this->adrressInstr(adr1, pos, size);
            op2 = this->adrressInstr(adr2, pos, size);
            position = pos;

            op2 = op2 ^ op1;
            psw[Z_FLAG] = op2 == 0 ? 1 : 0;
            psw[N_FLAG] = (op2 & 0x8000) ? 1 : 0;

            this->setResult(adr2, op2, position);
            break;
        case test:
            op1 = this->adrressInstr(adr1, pos, size);
            op2 = this->adrressInstr(adr2, pos, size);
            op2 = op2 & op1;
            psw[Z_FLAG] = op2 == 0 ? 1 : 0;
            psw[N_FLAG] = (op2 & 0x8000) ? 1 : 0;
            break;
        case shl:
            op1 = this->adrressInstr(adr1, pos, size);
            op2 = this->adrressInstr(adr2, pos, size);
            op2 = op1 << op2;
            sgn = (op2 & 0x8000) ? 1 : 0;
            psw[Z_FLAG] = op2 == 0 ? 1 : 0;
            psw[N_FLAG] = (op2 & 0x8000) ? 1 : 0;
            psw[C_FLAG] = (op2 & 0x10000) ? 1 : 0;

            this->setResult(adr2, op2, position);
            break;
        case shr:
            op1 = this->adrressInstr(adr1, pos, size);
            op2 = this->adrressInstr(adr2, pos, size);
            op2 = op1 >> op2;
            sgn = (op2 & 0x8000) ? 1 : 0;
            psw[Z_FLAG] = op2 == 0 ? 1 : 0;
            psw[N_FLAG] = (op2 & 0x8000) ? 1 : 0;
            psw[C_FLAG] = (op2 & 0x10000) ? 1 : 0;

            this->setResult(adr2, op2, position);
            break;
        default:
            std::cout << "did not recognize the OC" << std::endl;
            break;
        }
    }
}

void Emulator::jump(int adrress)
{
	PC =adrress; 
}

int Emulator::adrressInstr(int &adr, int &position, int size)
{
    int word = memory[PC++];
    int adrType = (word & ADR_TYPE) >> 5;
    switch (adrType)
    {
    case immed:
        adr = 0;
        if (!size)
            return memory[PC++];
        else
            return (memory[PC++] | (memory[PC++] << 8));
        break;

    case regdir:
        adr = 1;
        position = (word & REG_NUM) >> 1;
        return reg[position];
        break;

    case regind:
        adr = 2;
        position = reg[(word & REG_NUM) >> 1];
        return memory[position] | memory[position+1]<<8;
        break;

    case regindpom:
        adr = 3;
        position = reg[(word & REG_NUM) >> 1] + (memory[PC++] | (memory[PC++] << 8));
        return memory[position] | memory[position+1]<<8;
        break;

    case mem:
        adr = 4;
        position = (memory[PC++] | (memory[PC++] << 8));
        return memory[position] | memory[position+1]<<8;
        break;

    default:
        std::cout << "did not recognize adressInstr" << std::endl;
        exit(-1);
        break;
    }
}

void Emulator::setResult(int adr, int op, int pos)
{
    switch (adr)
    {
    case immed:
        Emulator::invalidInstr = true;
        break;
    case regdir:
        reg[pos] = op;
        break;
    case regind:
        memory[pos] = op;
        break;
    case regindpom:
        memory[pos] = op;
        break;
    case mem:
        memory[pos] = op;
        break;
    default:
        std::cout << "did not recognize setResult" << std::endl;
        exit(-1);
        break;
    }
}

void Emulator::interrupt()
{
   if(interrupted) return;

    if (!initialized)
    { 
        initialized = true;

        memory[--SP] = (PC >> 8) & 0xff;
        memory[--SP] = PC & 0xff;

        memory[--SP] = bitsetToCharHigher(psw);
        memory[--SP] = bitsetToCharLower(psw);

        PC = 0;
        PC = memory[IV_TABLE + 0 * TR_ENTRY] | (memory[IV_TABLE + 0 * TR_ENTRY + 1]<<8);
     
        int num1=memory[SP], num2=memory[SP+1],num3=memory[SP+2],num4=memory[SP+3];
        std::cout<<num1<<"+"<<num2<<"+"<<num3<<"+"<<num4<<std::endl;
  
    }
    if (Emulator::invalidInstr)
    { //za instr ulaz 1
        Emulator::invalidInstr = false;
        interrupted = true;
    
        memory[--SP] = (PC >> 8) & 0xff;
        memory[--SP] = PC & 0xff;

        memory[--SP] = bitsetToCharHigher(psw);
        memory[--SP] = bitsetToCharLower(psw);

        PC = 0;
        PC = memory[IV_TABLE + 1 * TR_ENTRY] | (memory[IV_TABLE + 1 * TR_ENTRY + 1]<<8);
     
        int num1=memory[SP], num2=memory[SP+1],num3=memory[SP+2],num4=memory[SP+3];
        std::cout<<num1<<"+"<<num2<<"+"<<num3<<"+"<<num4<<std::endl;
      
    }
    if (!psw[I_FLAG] && !psw[TR_FLAG] && Emulator::timerIntr)
    {
        interrupted = true;
        Emulator::timerIntr = false;
    
        memory[--SP] = (PC >> 8) & 0xff;
        memory[--SP] = PC & 0xff;

        memory[--SP] = bitsetToCharHigher(psw);
        memory[--SP] = bitsetToCharLower(psw);

        PC = 0;
        PC = memory[IV_TABLE + 2 * TR_ENTRY] | (memory[IV_TABLE + 2 * TR_ENTRY + 1]<<8);
     
    }
    if (!psw[I_FLAG] && !psw[TL_FLAG] && Emulator::consoleIntr)
    {
        interrupted = true;
        Emulator::consoleIntr = false;
        
        memory[--SP] = (PC >> 8) & 0xff;
        memory[--SP] = PC & 0xff;

        memory[--SP] = bitsetToCharHigher(psw);
        memory[--SP] = bitsetToCharLower(psw);

        PC = 0;
        PC = memory[IV_TABLE + 3 * TR_ENTRY] | (memory[IV_TABLE + 3 * TR_ENTRY + 1]<<8);
     
    }
  //  _mutex.unlock();
}

char Emulator::bitsetToCharLower(std::bitset<16> p)
{
    char c = 0x00;
    c |= p[0];
    c |= p[1] << 1;
    c |= p[2] << 2;
    c |= p[3] << 3;
    c |= p[4] << 4;
    c |= p[5] << 5;
    c |= p[6] << 6;
    c |= p[7] << 7;
    return c;
}

char Emulator::bitsetToCharHigher(std::bitset<16> p)
{
    char c = 0x00;
    c |= p[8];
    c |= p[9] << 1;
    c |= p[10] << 2;
    c |= p[11] << 3;
    c |= p[12] << 4;
    c |= p[13] << 5;
    c |= p[14] << 6;
    c |= p[15] << 7;
    return c;
}

void Emulator::enableRawMode()
{
  tcgetattr(STDIN_FILENO, &orig_termios);
  //atexit(disableRawMode);
  struct termios raw = orig_termios;
  raw.c_lflag &= ~(ECHO | ICANON);
  tcsetattr(STDIN_FILENO, TCSAFLUSH, &raw);
}

void Emulator::disableRawMode()
{
  tcsetattr(STDIN_FILENO, TCSAFLUSH, &orig_termios);
}