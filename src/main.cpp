#include <iostream>
#include <regex>
#include <vector>
#include <string>
#include "linker.h"
#include "Emulator.h"
#include "secAdr.h"

std::vector<SecAdr> secAdr; //smth u read from cmd line

int main(int argc, char **argv)
{
  std::regex _place = std::regex("(-place=)(.*)(@)(.*)"); //, objFile;
  std::regex objFile = std::regex(".*\\.o");
  std::smatch m;
  std::vector<std::string> files;

  static Linker linker;

  for (int i = 1; i < argc; i++)
  {

    std::string toMatch = argv[i];

    if (std::regex_search(toMatch, m, _place))
    {
      unsigned long int adr = std::stoi(m.str(4), nullptr, 16);

      if(m.str(2) == "iv_table" && adr != 0) std::cout << "iv_table mora poceti na adresu 0" << std::endl;
     
      for (int i = 0; i < secAdr.size(); i++)
        if (secAdr[i].getSection() == m.str(2))
        {
          std::cout << "Vise puta ste definisali adresu za " << m.str(2) << " sekciju" << std::endl;
        }

      secAdr.push_back(SecAdr(m.str(2), adr));
    }
    else if (std::regex_search(toMatch, m, objFile))
    {

      files.push_back(m.str(0));
      //linker.addFile(m.str(0));
    }
    else
    {

      std::cout << "NO MATCH, INPUT NOT VALID" << std::endl;
      exit(-1);
    }
  }
  linker.addFile(files);
  
  Emulator _emulator = Emulator();
  _emulator.exec();

  return 0;
}