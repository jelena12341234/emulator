#pragma once
#include <vector>
#include <string>
#include <unordered_map>
#include "Node.h"
#include "secAdr.h"
#include <fstream>
#include "relocs.h"

class SecAdress{    //pomeraj sekcija
public:
    std::string name;
    int address;
    int size;
    std::string relSec;     //if reltext\/reldata

    SecAdress(std::string n, int a, int s){
        name = n;
        address = a;
        size = s;
    }
};

class SecData{
public:
    char instr;
    std::string section;

    SecData(char d, std::string s){
        instr = d;
        section = s;
    }
};


extern std::vector<SecAdr> secAdr;

class Linker
{
    std::vector<std::string> files;

    static int numOfSections;
    static int jedId;

    std::unordered_map<std::string, Node> symTable; //unique symTable for whole program
    std::vector<std::ifstream> fl;                  //opened files
    std::vector<std::unordered_map<std::string, Node>> symTables; //symTables from each file
    std::vector<std::vector<Relocs>> relocs;
    std::vector<std::vector<SecAdress>> adresses;
    std::unordered_map<std::string,std::vector<SecData>> memory;

    static int _rip;

public:
    static char* _fMemory;
    static int len;

    friend class Emulator;

    void addFile(std::vector<std::string> objFiles)
    {
        files = objFiles;
        linking();
        close();
    }

    bool doesExist(std::string name)
    {
        if (symTable.find(name) == symTable.end())
            return false;
        else
            return true;
    }

    bool doesSecExist(std::string name)
    {
        if (memory.find(name) == memory.end())
            return false;
        else
            return true;
    }
  
    int maxPlaceArg()
    {
        int max = 0;

        for(int i=0; i<secAdr.size(); i++)
            if(secAdr[i].getAddress() > max) 
                max = secAdr[i].getAddress();
        
        return max;
    }

    int symTableVal(std::string name)
    {
        if(secPlaceExists(name))
         return secPlaceAdress(name);
            else return maxPlaceArg();
    }



    bool secPlaceExists(std::string section)
    {
        bool flag = false;
        for(int i=0; i<secAdr.size(); i++)
            if(secAdr[i].getSection() == section) flag = true;
        
        return flag;
    }

    int secPlaceAdress(std::string section)
    {
        int adr = -1;   //greska
        for(int i=0; i<secAdr.size(); i++)
            if(secAdr[i].getSection() == section) adr  = secAdr[i].getAddress();

        return adr;
    }

    void linking();

    void close();

    void link();

    void updateIDs(int id);

    std::string findSection(int i, int section);
};
