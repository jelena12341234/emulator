#include "timer.h"
#include <thread>
#include <chrono>
#include "Emulator.h"
#include <iostream>


Timer::Timer()
{
    _thread = std::thread(&Timer::timer,this);   
    //std::cout<<"nit inicijalizovana"<<std::endl;
}

int Timer::getTimerInterval()
{
    timer_cfg = Emulator::memory[0xff10] | (Emulator::memory[0xff11])<<8;
    int res = timer_cfg & 0x7;
    switch (res)
    {
    case 0x0:
        return 500;
        break;
    case 0x1:
        return 1000;
        break;
    case 0x2:
        return 1500;
        break;
    case 0x3:
        return 2000;
        break;
    case 0x4:
        return 5000;
        break;
    case 0x5:
        return 10000;
        break;
    case 0x6:
        return 30000;
        break;
    case 0x7:
        return 60000;
        break;
    default:
        break;
    }
}

void Timer::timer()
{

    while (!Emulator::halted)
    {
        
        std::this_thread::sleep_for(std::chrono::milliseconds(getTimerInterval()));
        Emulator::timerIntr = true;
    }
}