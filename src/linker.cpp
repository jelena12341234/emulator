#include "linker.h"
#include "secAdr.h"
#include <fstream>
#include <iostream>
#include <sstream>
#include <string>
#include <regex>
#include "relocs.h"

int Linker::numOfSections = 0;
int Linker::jedId = 1;
char *Linker::_fMemory = nullptr;
int Linker::len = 0;
int Linker::_rip = 0;

void Linker::linking()
{
    for (int i = 0; i < this->files.size(); i++)
        this->fl.push_back(std::ifstream(files[i]));

    link();

    for(auto it = symTable.begin(); it != symTable.end(); it++)
    {
        std::cout << it->first << "##" << it->second.section <<"##" << it->second.value <<"##" << it->second.size<<std::endl;
    }


}

void Linker::close()
{
    for (int i = 0; i < this->fl.size(); i++)
        fl[i].close();
}


void Linker::link()
{

    std::regex rel = std::regex("(#rel\\.)(.*)");
    std::regex relDataPC = std::regex("([0-9][0-9]*)(\\s+)(R_PC16)(\\s+)([0-9][0-9]*)");
    std::regex relDataR = std::regex("([0-9][0-9]*)(\\s+)(R_16)(\\s+)([0-9][0-9]*)");
    std::regex data = std::regex("([0-9a-f][0-9a-f])+");
    std::regex secData = std::regex("(<)(.*)(>:)");
    std::regex symbols = std::regex("(.*)(\\t)(.*)(\\t)(([0-9][0-9]*))(\\t)(([0-9][0-9]*))(\\t)([lg])");
    std::smatch m;
    std::string currRel = "err";
    std::string currData = "err";

    for (int fileNo = 0; fileNo < this->fl.size(); fileNo++)
    {
        Node::jedId = 1;

        std::unordered_map<std::string, Node> umap;
        std::string line;
        std::vector<Relocs> myRelocs;
        std::vector<SecAdress> secAdr;

        int cnt = 0;
        
        while (std::getline(fl[fileNo], line))
        {
            cnt++;
            if (cnt < 3)
                continue;

            if (std::regex_search(line, m, secData))
            {
                currData = m.str(2);
                continue;
            } else if (std::regex_search(line, m, symbols))
            {
      
                std::istringstream iss(line), iss1(line);
                std::string serialNum, name, UNDsection;
                int value, size, section;
                char lVg; //local V global

                if (!(iss >> name >> section >> value >> size >> lVg))
                { //proveri da li je hex broj
                    if (!(iss1 >> name >> UNDsection >> value >> size >> lVg))
                    {
                        std::cout << "ERROR WHILE READING VALUES FROM LINE: " << line << std::endl;
                        exit(-1);
                    }
                    else
                    {
                        umap.insert(std::make_pair(name, Node(name, UNDsection, value, size, lVg)));
                        if (!this->doesExist(name))
                        {
                            if (lVg == 'l' && size > 0)
                            {
                                std::cout << "UND cant have size" << std::endl;
                                exit(-3);
                            }
                            else
                            {
                                this->symTable.insert(std::make_pair(name, Node(name, UNDsection, value, size, lVg)));
                                Node::jedId--;
                            }
                        }
                    }
                }
                else
                {
                        
                        if (lVg == 'l' && size > 0 && secPlaceExists(name))
                            value = secPlaceAdress(name);
                        else if (lVg == 'l' && size > 0 && !secPlaceExists(name)) value = maxPlaceArg();
                        if (lVg == 'l' && size > 0)
                        std::cout<<"sekcija "<<name << " je na adresi "<<value<<std::endl;
                    
                    if (!this->doesExist(name))
                    {
                        umap.insert(std::make_pair(name, Node(name, section, value, size, lVg)));
                        if (lVg == 'l' && size > 0)
                        {
                            numOfSections++;
                            //   this->updateIDs(++this->numOfSections);
                            int val = -1;
                            for (auto it = symTable.begin(); it != symTable.end(); ++it)
                                if (it->second.section == this->numOfSections - 1 && it->second.local && it->second.size > 0 && symTableVal(it->first)==symTableVal(name)) // ovde bih dodala ako su poravnjanja sekcija isti
                                    val = it->second.value + it->second.size;



                            this->symTable.insert(std::make_pair(name, Node(name, this->numOfSections, val != -1 ? val : symTableVal(name), size, 'l')));   //kada je val == -1 onda poravnanje sekcije
                            Node::jedId--;
                            this->symTable.at(name).serialNum = this->jedId++;

                            int adr = symTableVal(name);    //i ovde bih stavila poravnanje sekicje
                            for (int i = 0; i < adresses.size(); i++)
                            {
                                std::vector<SecAdress> sa = adresses[i];
                                for (int j = 0; j < sa.size(); j++)
                                    if (symTable.at(name).section > symTable.at(sa[j].name).section && symTableVal(sa[j].name)==symTableVal(name))
                                        adr = symTable.at(sa[j].name).value + symTable.at(sa[j].name).size;
                            }

                            for (int i = 0; i < secAdr.size(); i++)
                                if (symTable.at(name).section > symTable.at(secAdr[i].name).section && symTableVal(secAdr[i].name)==symTableVal(name))
                                    adr = symTable.at(secAdr[i].name).value + symTable.at(secAdr[i].name).size;

                            secAdr.push_back(SecAdress(name, adr, size));
                        }
                        else
                        {

                            for (auto it = umap.begin(); it != umap.end(); ++it)
                                if (it->second.section == section && it->second.size > 0 && it->second.local)
                                {
                                    int val = this->symTable.at(it->first).value + this->symTable.at(it->first).size - it->second.size + value;
                                    this->symTable.insert(std::make_pair(name, Node(name, this->symTable.at(it->first).section, val, 0, 'g')));
                                    Node::jedId--;
                                    this->symTable.at(name).serialNum = this->jedId++;
                                }
                        }
                    }
                    else
                    {
                        umap.insert(std::make_pair(name, Node(name, section, value, size, lVg)));

                        if (lVg == 'l' && size > 0)
                        {
                            this->symTable.at(name).size = this->symTable.at(name).size + size;
                            for (auto it = symTable.begin(); it != symTable.end(); ++it)
                                if (it->second.section > symTable.at(name).section)
                                    symTable.at(it->first).value = it->second.value + size;

                            int adr1 = 0, adr2 = 0;
                            for (int i = 0; i < adresses.size(); i++)
                            {
                                std::vector<SecAdress> sa = adresses[i];
                                for (int j = 0; j < sa.size(); j++)
                                    if (sa[j].name == name)
                                        adr1 = sa[j].address + sa[j].size;
                            }
                            secAdr.push_back(SecAdress(name, adr1, size));

                            for (int i = 0; i < adresses.size(); i++)
                            {
                                std::vector<SecAdress> sa = adresses[i];
                                for (int j = 0; j < sa.size(); j++)
                                    if (symTable.at(sa[j].name).section > symTable.at(name).section)
                                    {
                                        sa[j].address += size;
                                        adresses[i] = sa;
                                    }
                            }
                        }
                        else
                        {
                            if (this->symTable.at(name).UNDsection == "UND")
                            {
                                for (auto it = umap.begin(); it != umap.end(); ++it)
                                    if (it->second.section == section && it->second.size > 0 && it->second.local)
                                    {
                                        int val = this->symTable.at(it->first).value + this->symTable.at(it->first).size - it->second.size + value;
                                        this->symTable.at(name).value = val;
                                        this->symTable.at(name).section = symTable.at(it->first).section;
                                        this->symTable.at(name).serialNum = this->jedId++;
                                    }
                            }
                            else
                            {
                                std::cout << "Error, symbol " << name << " already defined" << std::endl;
                                exit(-2);
                            }
                        }
                    }
                }
    
                continue;
            }
            else if (std::regex_search(line, m, rel))
            {
                currRel = m.str(2);
                continue;
            }
            else if (std::regex_search(line, m, relDataPC))
            {
                std::stringstream num1(m.str(1));
                int address = 0;
                num1 >> address;
                std::stringstream num2(m.str(5));
                int n = 0;
                num2 >> n;
                Relocs r(address, m.str(3), n, currRel);
                myRelocs.push_back(r);
                continue;
            }
            else if (std::regex_search(line, m, relDataR))
            {
                std::stringstream num1(m.str(1));
                int address = 0;
                num1 >> address;
                std::stringstream num2(m.str(5));
                int n = 0;
                num2 >> n;
                Relocs r(address, m.str(3), n, currRel);
                myRelocs.push_back(r);
                continue;
            }
            else if (std::regex_search(line, m, data))
            {
                std::string st = m.str(0);
                std::vector<SecData> sd;
                if (this->doesSecExist(currData))
                    sd = memory.at(currData);

                unsigned char c;
                for (int i = 0; i <= st.size() - 2; i += 2)
                {
                    unsigned char c1 = st[i];
                    int num1 = std::stoi(std::string(1, c1), 0, 16);
                    c1 = st[i + 1];
                    int num2 = std::stoi(std::string(1, c1), 0, 16);
                    int num = (num1 & 0xf) << 4 | (num2 & 0xf);
                    c = (unsigned char)(num & 0xff);

                    sd.push_back(SecData(c, currData));
                }
                if (this->doesSecExist(currData))
                    memory.at(currData) = sd;
                else
                    memory.insert(std::make_pair(currData, sd));
                continue;
            }
        }
        this->relocs.push_back(myRelocs);
        symTables.push_back(umap);
        this->adresses.push_back(secAdr);
    }

    for (int i = 0; i < relocs.size(); i++)
    {
        for (int j = 0; j < relocs[i].size(); j++)
        {
            if (relocs[i][j].typeReloc == "R_16")
            { //nadjes pomeraj sekcije data adresses[][].adress + relocs[][].adress -symTable(name).value
             
                for (auto it = symTables[i].begin(); it != symTables[i].end(); ++it)
                {
                    if (it->second.serialNum == relocs[i][j].serialNum)
                    {
                        
                        int adr = -1;
                        for (int k = 0; k < adresses[i].size(); k++)
                        {
                            if (adresses[i][k].name == relocs[i][j].relSec)
                            {
                                adr = adresses[i][k].address + relocs[i][j].address - symTable.at(adresses[i][k].name).value;
                            }
                        }

                        std::vector<SecData> sd = memory.at(relocs[i][j].relSec);

                        int mem = sd[adr].instr | (sd[adr + 1].instr << 8);
                        if (mem & 0x8000)
                        { //ako  je <0 treba postaviti sve keceve ispred
                            mem = mem | 0xffff0000;
                        }
                        int val = symTable.at(it->first).value + mem;
                        sd[adr] = SecData(val & 0xff, relocs[i][j].relSec);
                        sd[adr + 1] = SecData((val >> 8) & 0xff, relocs[i][j].relSec);
                        memory.at(relocs[i][j].relSec) = sd;
                    }
                }
            }
            else if (relocs[i][j].typeReloc == "R_PC16")
            { //x-*-2 x==value, *-adrress, -2 == pom
                for (auto it = symTables[i].begin(); it != symTables[i].end(); ++it)
                {
                    if (it->second.serialNum == relocs[i][j].serialNum)
                    {
                        int adr = -1;
                        for (int k = 0; k < adresses[i].size(); k++)
                        {
                            if (adresses[i][k].name == relocs[i][j].relSec)
                                adr = adresses[i][k].address + relocs[i][j].address - symTable.at(adresses[i][k].name).value;
                        }
                        std::vector<SecData> sd = memory.at(relocs[i][j].relSec);
                        int mem = sd[adr].instr | (sd[adr + 1].instr << 8);
                        if (mem & 0x8000)
                        { //ako  je <0 treba postaviti sve keceve ispred
                            mem = mem | 0xffff0000;
                        }
                        int val = symTable.at(it->first).value - adr + mem; //-2 treba da bude procitana vrednost

                        if (symTable.at(it->first).size > 0 && symTable.at(it->first).local) //??
                        {
                            for (int k = 0; k < adresses[i].size(); k++)
                                if (adresses[i][k].name == it->first)
                                    val = -adr + mem + adresses[i][k].address;
                            //val = -adr + mem + adresses[i][0].address;
                        }
                        
                        sd[adr] = SecData(val & 0xff, relocs[i][j].relSec);
                        sd[adr + 1] = SecData((val >> 8) & 0xff, relocs[i][j].relSec);
                        memory.at(relocs[i][j].relSec) = sd;
                    }
                }
            }
            else
            {
                std::cout << "uknown type reloc (i,j)==(" << i << "," << j << ")" << std::endl;
                exit(-5);
            }
        }
    }

    std::cout<<"MEMORY size "<<memory.size()<<std::endl;
    
    int sum = 0;
    for (auto it = memory.begin(); it != memory.end(); ++it)
        sum += it->second.size();

    Linker::_fMemory = new char[1 << 16];
    for (int i = 0; i < 1 << 16; i++)
        Linker::_fMemory[i] = 0;

    Linker::len = sum;
    int index = 0;
    int highestAdress = 0;
    _rip = 0;

    std::vector<std::string> sectionNames;


    for (int i = 0; i < secAdr.size(); i++)
    {
        if (doesSecExist(secAdr[i].getSection()))
        {
            int adr = secAdr[i].getAddress();
            if(secAdr[i].getSection() == "text") _rip = adr;
            std::vector<SecData> sd = memory.at(secAdr[i].getSection());
            for (int j = 0; j < sd.size(); j++)
                Linker::_fMemory[adr++] = sd[j].instr;
            sectionNames.push_back(secAdr[i].getSection());
            if(adr > highestAdress) highestAdress = adr;
        }
        else
            std::cout << "Ne postoji sekcija: " << secAdr[i].getSection() << std::endl;
    }

    for (auto it = memory.begin(); it != memory.end(); ++it)
    {
        bool found = false;
        for (int i = 0; i < sectionNames.size(); i++)
            if (sectionNames[i] == it->first)
            {
                found = true;
                break;
            }
        if (found)
            continue;

        if(it->first == "text") _rip = highestAdress;
        std::vector<SecData> sd = it->second;
        for (int i = 0; i < sd.size(); i++)
            Linker::_fMemory[highestAdress++] = sd[i].instr;
    }

    if(_rip == 0) 
    {
        std::cout<<"main not recognized"<<std::endl;
        exit(-1);
    }
}

void Linker::updateIDs(int id)
{
    for (auto it = symTable.begin(); it != symTable.end(); ++it)
    {
        if (it->second.jedId > id)
            it->second.jedId++;
    }
}
