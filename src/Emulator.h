#pragma once
#include <bitset>
#include "timer.h"
#include "keyboard.h"
#include <termios.h>
#include <vector>
#

#define Z_FLAG 0x0001
#define O_FLAG 0x0002
#define C_FLAG 0x0004
#define N_FLAG 0x0008
#define I_FLAG 0x8000
#define TL_FLAG 0x4000
#define TR_FLAG 0x2000

#define INSTR_SIZE 0x04
#define ADR_TYPE 0xe0
#define REG_NUM 0x1e

#define DATA_OUT 0xFF00
#define DATA_IN 0xFF02

#define MEM_SYZE 1 << 16

#define IV_TABLE 0x0000

#define TR_ENTRY 2
#define TL_ENTRY 3

#define PC reg[7]
#define SP reg[6]

enum instrucitons
{
    halt,
    iret,
    ret,
    intr,
    call,
    jmp,
    jeq,
    jne,
    jgt,
    push,
    pop,
    xchg,
    mov,
    add,
    sub,
    mul,
    div_,
    cmp,
    not_,
    and_,
    or_,
    xor_,
    test,
    shl,
    shr
};

enum adr
{
    immed,
    regdir,
    regind,
    regindpom,
    mem
};


class Emulator
{
    std::bitset<16> psw;
    unsigned short reg[8];

    static char *memory;

    bool initialized = false;
    static bool halted;
    static bool timerIntr;
    static bool consoleIntr;
    static bool invalidInstr;
    static unsigned short timer_cfg;
    
    Timer* _timer = nullptr;
    KeyBoard* _keyboard = nullptr;

    struct termios orig_termios;
    
    bool interrupted = false;

public:
    Emulator();

    ~Emulator();

    void jump(int adrress);

    friend class Timer;

    friend class KeyBoard;

    void interrupt();

    void exec();

    int adrressInstr(int &adr, int &position, int size); //position indicates which reg or which position in memory

    void setResult(int adr, int op, int pos);

    char bitsetToCharLower(std::bitset<16> p);

    char bitsetToCharHigher(std::bitset<16> p);

    void enableRawMode();

    void disableRawMode();
};