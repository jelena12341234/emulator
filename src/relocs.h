#pragma once
#include <string>

class Relocs{
public:
    int address;
    std::string typeReloc;
    int serialNum;
    std::string relSec; //if relData or relTxt

    Relocs(int a, std::string tr, int n, std::string rs){
        serialNum=n;
        typeReloc=tr;
        address=a;
        relSec = rs;
    }
};