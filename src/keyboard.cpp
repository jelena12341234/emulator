#include "keyboard.h"
#include "Emulator.h"
#include <thread>
#include <poll.h>
#include <unistd.h>
#include <iostream>

KeyBoard::KeyBoard()
{
 _thread = std::thread(&KeyBoard::eventListener,this);   
}

void KeyBoard::eventListener()
{
	while(!Emulator::halted) {

		int flag = poll(input, 1, 1);
		if (flag > 0) {
            Emulator::consoleIntr = true;
            char c;
	        read(0, &c, 1);
            std::cout<<"***"<<c<<std::endl;
            Emulator::memory[DATA_IN] = c;

		}
	}

}